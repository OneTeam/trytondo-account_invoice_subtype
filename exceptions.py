# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.exceptions import UserError
from trytond.model.exceptions import ValidationError


class InvoiceSubtypeErrorNegative(ValidationError):
    pass

class InvoiceSubtypeErrorPositive(ValidationError):
    pass

class InvoiceSubtypeNone(ValidationError):
    pass

class SequenceStrictErrorPrefix(ValidationError):
    pass

class SequenceStrictErrorSuffix(ValidationError):
    pass

class SequenceStrictErrorDate(ValidationError):
    pass

class SequenceStrictErrorNumber(ValidationError):
    pass
