from trytond.model import fields
from trytond.pool import PoolMeta


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'
    invoice_subtype_out = fields.Many2One(
        'account.invoice.subtype', "Customer Subtype",
        domain=[('type', '=', 'out')],
        depends=['type']
        )
    invoice_subtype_in = fields.Many2One(
        'account.invoice.subtype', "Provider Subtype",
        domain=[('type', '=', 'in')],
        depends=['type']
        )
