from trytond.model import fields, ModelView, ModelSQL, DeactivableMixin, Unique, ValueMixin
from trytond.pyson import Eval, If
from trytond.pool import  Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.modules.company.model import(
    CompanyMultiValueMixin, CompanyValueMixin)
from trytond.modules.account_invoice.exceptions import  InvoiceNumberError
from .exceptions import InvoiceSubtypeErrorNegative, InvoiceSubtypeErrorPositive, InvoiceSubtypeNone
from trytond.i18n import gettext
from trytond.exceptions import UserError


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'

    subtype = fields.Many2One(
        'account.invoice.subtype', 'Subtype',
        domain=[('type', '=', Eval('type'))
            ],
        states={
            'readonly': ((Eval('state') != 'draft')
                         | Eval('context', {}).get('subtype')
                         | (Eval('lines', [0]) & Eval('subtype'))),
        }, depends=['state', 'lines', 'type'],
        ondelete='RESTRICT')

    credit_note = fields.Boolean(
        'Credit Note',
        context={
            'credit_note': Eval('credit_note'),
        })
    invoice_resolution = fields.Many2One(
        'account.invoice.resolution', 'Invoice Resolution',
        domain=[],
        ondelete='RESTRICT')

    @staticmethod
    def default_subtype():
        return Transaction().context.get('subtype', None)

    @staticmethod
    def default_credit_note():
        return Transaction().context.get('credit_note', False)

    @fields.depends('party', 'type', 'accounting_date', 'invoice_date')
    def on_change_with_subtype(self):
        subtype = None
        if self.party:
            with Transaction().set_context(
                    date=self.accounting_date or self.invoice_date):
                return subtype.id if subtype else self.invoice_default_subtype(self.type)
        else:
            return None
        
    def invoice_default_subtype(cls, type):
        pool = Pool()
        Configurations = pool.get('account.configuration')
        Configuration = Configurations(1)
        subtype = None
        
        if cls.type == 'out':
            if cls.party.invoice_subtype_out:
                subtype = cls.party.invoice_subtype_out
            else:
                subtype = Configuration.default_invoice_subtype_out
        if cls.type == 'in':
            if cls.party.invoice_subtype_in:
                subtype = cls.party.invoice_subtype_in
            else:
                subtype = Configuration.default_invoice_subtype_in
        return subtype.id if subtype else None
    
    def get_next_number(self, pattern=None):
        "Return invoice number and sequence used"
        pool = Pool()
        Sequence = pool.get('ir.sequence.strict')
        Period = pool.get('account.period')
        Account_Configuration = pool.get('account.configuration')
        configuration = Account_Configuration(1)

        if not self.subtype:
            if self.type == 'out':
                default_subtype = configuration.default_invoice_subtype_out
                default_party_subtype = self.party.invoice_subtype_out
                if default_party_subtype:
                    self.subtype = default_party_subtype
                elif default_subtype:                    
                    self.subtype = default_subtype
                else:
                    raise InvoiceSubtypeNone(
                        gettext('account_invoice_subtype.msg_subtype_none'))
            if self.type == 'in':
                default_subtype = configuration.default_invoice_subtype_in
                default_party_subtype = self.party.invoice_subtype_in
                if default_party_subtype:
                    self.subtype = default_party_subtype
                elif default_subtype:                    
                    self.subtype = default_subtype
                else:
                    raise InvoiceSubtypeNone(
                        gettext('account_invoice_subtype.msg_subtype_none'))
        
        credit_note = Transaction().context.get('credit_note', False)

        if pattern is None:
            pattern = {}
        else:
            pattern = pattern.copy()

        accounting_date = self.accounting_date or self.invoice_date
        period_id = Period.find(
            self.company.id, date=accounting_date,
            test_state=self.type != 'in')

        period = Period(period_id)
        fiscalyear = period.fiscalyear
        pattern.setdefault('company', self.company.id)
        pattern.setdefault('fiscalyear', fiscalyear.id)
        pattern.setdefault('period', period.id)
        invoice_type = self.type
        if (all(l.amount <= 0 for l in self.lines if l.product)
                and self.total_amount < 0):
            invoice_type += '_credit_note'
            #verify credit_note in context
            if 'credit_note' in Transaction().context:
                if not credit_note:
                    raise InvoiceSubtypeErrorNegative(
                        gettext('account_invoice_subtype.msg_invoice_negative'))
            self.credit_note = True
            if self.subtype.sequence_credit_note:
                sequence = self.subtype.sequence_credit_note
            
        else:
            invoice_type += '_invoice'
            # verify credit_note in context
            if credit_note:
                raise InvoiceSubtypeErrorPositive(
                        gettext('account_invoice_subtype.msg_invoice_positive'))
            if self.subtype.sequence:
                sequence = self.subtype.sequence
        if not sequence:
            for invoice_sequence in fiscalyear.invoice_sequences:
                if invoice_sequence.match(pattern):
                    sequence = getattr(
                        invoice_sequence, '%s_sequence' % invoice_type)
                    break
            else:
                raise InvoiceNumberError(
                    gettext('account_invoice.msg_invoice_no_sequence',
                        invoice=self.rec_name,
                        fiscalyear=fiscalyear.rec_name))
        if sequence.invoice_resolution:
            sequence.valid_invoice_resolution()
            self.invoice_resolution = sequence.invoice_resolution
        with Transaction().set_context(date=accounting_date):
            return Sequence.get_id(sequence.id), sequence

    @fields.depends('type', 'subtype')
    def on_change_type(self):
        Journal = Pool().get('account.journal')
        if self.subtype and self.subtype.default_journal:
            journals = [self.subtype.default_journal]
        else:
            journal_type = {
                'out': 'revenue',
                'in': 'expense',
            }.get(self.type or 'out', 'revenue')
            journals = Journal.search([
                ('type', '=', journal_type),
            ], limit=1)
        if journals:
            self.journal, = journals


class InvoiceSubtype(DeactivableMixin, ModelSQL, ModelView):
    'Invoice Subtype'
    __name__ = 'account.invoice.subtype'

    _journal_type = {
        'out': 'revenue',
        'in': 'expense',
    }

    name = fields.Char('Name', required=True)
    code = fields.Char('Code', required=True)
    type = fields.Selection([
        ('out', "Customer"),
        ('in', "Supplier"),
    ], "Type", select=True, required=True)
    sequence = fields.Many2One('ir.sequence.strict', "Sequence",
                               ondelete='RESTRICT')
    sequence_credit_note = fields.Many2One('ir.sequence.strict', "Sequence Credit Note",
                                           ondelete='RESTRICT')
    default_journal = fields.Many2One('account.journal', 'Journal',
                                      domain=[
                                          If(Eval('type') == 'out',
                                             ('type', '=', 'reveneu'),
                                             ('type', '=', 'expense'))
                                      ],
                                      depends=['type'],
                                      ondelete='RESTRICT')
    report_title = fields.Char('Report Title', translate=True)
    report_text = fields.Text('Report Text', translate=True)

    @classmethod
    def __setup__(cls):
        super(InvoiceSubtype, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('name_unique', Unique(t, t.name),
             'account_invoice_subtype.msg_invoice_subtype_name_unique'),
            ('code_unique', Unique(t, t.code),
             'account_invoice_subtype.msg_invoice_subtype_code_unique'),
            ]

class InvoiceSubtypeFilter(ModelView):
    'Invoice Subtype Filter'
    __name__ = 'account.invoice.subtype_filter'

    subtype = fields.Many2One('account.invoice.subtype', 'Subtype')
    credit_note = fields.Boolean('Credit Note')


class InvoiceResolution(DeactivableMixin, ModelSQL, ModelView):
    'Invoice Resolution'

    __name__ = 'account.invoice.resolution'

    name = fields.Char('Name', required=True)
    resolution_number = fields.Char('Resolution Number')
    from_number = fields.Integer('From Number')
    to_number = fields.Integer('To Number')
    prefix = fields.Char('Prefix')
    suffix = fields.Char('Suffix')
    report_text = fields.Text('Report Text', translate=True)
    valid_date_time_from = fields.Date('Valid Date Time From')
    valid_date_time_to = fields.Date('Valid Date Time To')
    
