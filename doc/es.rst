Account Invoice Expenses Module
###############################

Agrega el modelo Subtipos (account.invoice.subtype) el cuál cataloga las *Facturas* (Invoices) y está formado por los campos:

* Nombre.
* Código.
* Activo.
* Tipo: una de dos opciones *Cliente* (in) o *Proveedor* (out).
* Secuencia: Si se indica será utilizada en vez de la secuencia de las Facturas.
* Secuencia nota credito: Si se indica sera utilizada en vez de la secuencia de Abonos a factura.
* Diario por defecto: Si se indica utilizará este diario por defecto y no el utilizado en la configuración de Facturas.

Se creán los subtipos usados en colombia permitiendo el ajuste por los usarios (@todo aunque estos deberían moverse a un nuevo módulo invoice_subtype):

* Gastos
  
  * Código: exp
  * Tipo: Proveedor (out)

* Documento equivalente:
  
  * Codigo: DEP
  * Tipo: Proveedor (out)

Se Ajusta el menú de las facturas para tener acceder a las ventanas clasificadas de las Facturas de acuerdo a los anteriores subtipos creados quedandó asi:

* Facturas Proveedor (Acceso a todas las facturas sin importar el subtipo)
  
  * Facturas (Acceso a las facturas sin subtipo)
  * Gastos (Acceso a las facturas de Proveedor del subtipo Gasto)
  * Documentos Equivalentes (Acceso a las facturas de proveedores el subtipo Documento Equivalente)
  * Otros documentos (Acceso a las facturas con subtipo diferente a Gastos y Documentos Equivalente)

* Facturas Clieentes (Acceso a todas las facturas sin importar el subtipo)

  * Facturas (Acceso a las facturas sin subtipo)
  * Otros documentos (Acceso a las facturas con subtipo)

Se modifican las secuencias agregando (@todo debería ir en otro módulo ir_sequence_limit):

* Máximo: si se establece y el siguiente número a entregar lo supera arrojará error.
* Fecha de vencimiento: si se establece y se solicita el siguiente número por encima de la fecha arrojará error.
