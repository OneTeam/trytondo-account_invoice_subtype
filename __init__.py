from trytond.pool import Pool
from .invoice import *
from .sequence import *
from .configuration import *
from .party import *


def register():
    Pool.register(
        Invoice,
        InvoiceSubtype,
        InvoiceSubtypeFilter,
        InvoiceResolution,
        Sequence,
        SequenceStrict,
        Configuration,
        Party,
        module='account_invoice_subtype', type_='model')
