from trytond.model import fields
from trytond.pool import PoolMeta


class Configuration(metaclass=PoolMeta):
    __name__ = 'account.configuration'
    default_invoice_subtype_out = fields.Many2One(
        'account.invoice.subtype', "Customer Subtype",
        domain=[('type', '=', 'out')],
        depends=['type'])
    default_invoice_subtype_in = fields.Many2One(
        'account.invoice.subtype', "Provider Subtype",
        domain=[('type', '=', 'in')],
        depends=['type'])
