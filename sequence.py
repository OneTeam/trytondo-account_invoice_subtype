
from trytond.model import fields
from trytond.pyson import Eval
from trytond.pool import  Pool, PoolMeta
from .exceptions import SequenceStrictErrorDate, SequenceStrictErrorNumber, \
    SequenceStrictErrorSuffix, SequenceStrictErrorPrefix
from trytond.i18n import gettext
from trytond.exceptions import UserError

from datetime import date

class Sequence(metaclass=PoolMeta):
    __name__ = 'ir.sequence'
    invoice_resolution = fields.Many2One(
        'account.invoice.resolution', 'Invoice Resolution',
        domain=[('prefix', '=', Eval('prefix')),
                ('suffix', '=', Eval('suffix')),
                ('active', '=', True)],
        ondelete='RESTRICT')

    def valid_invoice_resolution(self):
        """Valida la resolucion de facturacion
        debe fallar si no pasa algún criterio"""
        Date = Pool().get('ir.date')
        ir = self.invoice_resolution
        if ir:
            if self.prefix != ir.prefix:
                raise SequenceStrictErrorPrefix(
                    gettext('account_invoice_expenses.msg_sequence_prefix'))
            if self.suffix != ir.suffix:
                raise SequenceStrictErrorSuffix(
                    gettext('account_invoice_expenses.msg_sequence_suffix'))
            if ir.valid_date_time_from:
                if Date.today() < ir.valid_date_time_from:
                    raise SequenceStrictErrorDate(
                        gettext('account_invoice_expenses.msg_sequence_date'))
            if ir.valid_date_time_to:
                if Date.today() > ir.valid_date_time_to:
                    raise SequenceStrictErrorDate(
                        gettext('account_invoice_expenses.msg_sequence_date'))
            if ir.from_number:
                if self.number_next_internal < ir.from_number:
                    raise SequenceStrictErrorNumber(
                        gettext(
                            'account_invoice_expenses.msg_sequence_number'))
            if ir.to_number:
                if self.number_next_internal > ir.to_number:
                    raise SequenceStrictErrorNumber(
                        gettext(
                            'account_invoice_expenses.msg_sequence_number'))

class SequenceStrict(Sequence):
    __name__ = 'ir.sequence.strict'

